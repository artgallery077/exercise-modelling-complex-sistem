## Omniqueue: Ticketing System

**Status: In Development, not ready for use**

## You need to implement a ticketing system for an office (bank, post office, or medical center) that works on different devices, each with a different purpose:

- Touchscreen tablet for requesting a ticket for a specific service

- Large display that shows the tickets in queue and the next 2 tickets to be called

- Internal web app that displays the queued tickets and allows for calling the next ticket

- Mobile web app for users, available both in logged-in mode, where the user associates their ticket with their personal account (with ticket history), and in logged-out mode, where they only enter the ticket ID and get information about the estimated time and queue length.


## How do you think a system like this should be structured? Which technology stack would you use? 

To further improve the scalability and maintainability of the Omniqueue Ticketing System, a microservices architecture can be adopted. This will allow for the system to be broken down into smaller, independent services that can be developed, deployed, and scaled separately.

More info: 
    `__Omniqueue_Plan.yaml`
    `_Micro_Service.yaml`
    `_UserJourneys.yaml`
